var sanPham = new SanPhamService();

function getELE(id) {
    return document.getElementById(id);
}

function getListProducts() {
    sanPham.layDSSP()
        .then(function (result) {
            console.log(result.data);
            renderTable(result.data);
            setLocalStorage(result.data);
        })
        .catch(function (error) {
            console.log(error);
        });
}
getListProducts();

function setLocalStorage(mangSP) {
    localStorage.setItem("DSSP", JSON.stringify(mangSP));
}


getELE("basic-addon2").addEventListener("click", function () {
    var mangSP = getLocalStorage();
    var mangTK = [];
    console.log(mangSP);

    var chuoiTK = getELE("inputTK").value;

    mangTK = sanPham.timKiemSP(mangSP, chuoiTK);

    console.log(mangTK);
    renderTable(mangTK);

});

function getLocalStorage() {
    var mangKQ = JSON.parse(localStorage.getItem("DSSP"));
    return mangKQ
}


getELE("btnThemSP").addEventListener("click", function () {
    var footerEle = document.querySelector(".modal-footer");
    footerEle.innerHTML = `
     <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              Close
            </button>
        <button onclick="addProducts()" class="btn btn-primary">Add Product</button>
    `;
});


function renderTable(mangSP) {
    var content = "";
    var count = 1;
    mangSP.map(function (sp, index) {
        content += `
            <tr>
                <td>${count}</td>
                <td>${sp.name}</td>
                <td>${sp.price}</td>
                <td>${sp.img}</td>
                <td>${sp.desc}</td>
                <td>
                    <button class="btn btn-danger" onclick="xoaSP('${sp.id}')">Xóa</button>
                    <button class="btn btn-info" onclick="xemSP('${sp.id}')">Xem</button>
                </td>
            </tr>
        `;
        count++;
    });
    getELE("tblDanhSachSP").innerHTML = content;
}

function addProducts() {

    name = getELE("TenSP").value;

    price = getELE("GiaSP").value;

    img = getELE("HinhSP").value;

    desc = getELE("MoTa").value;

    var sp = new SanPham(name, price, img, desc);
    console.log(sp);

    var isValid =
        validation.kiemTraRong(
            name,
            "spanName",
            "Vui Lòng Nhập Tên Sản Phẩm"
        )

    isValid =
        isValid &
        validation.kiemTraRong(
            price,
            "spanPrice",
            "Vui Lòng Nhập Giá Sản Phẩm"
        ) &&
        validation.kiemTraGia(
            price,
            "spanPrice",
            "Giá phải từ 1,000,000VND-100,000,000VND",
            1000000,
            100000000
        );
    isValid =
        isValid &
        validation.kiemTraRong(
            img,
            "spanImg",
            "Vui Lòng Nhập Hình Ảnh Sản Phẩm"
        );
    isValid =
        isValid &
        validation.kiemTraRong(
            desc,
            "spanDesc",
            "Vui Lòng Nhập Mô Tả Sản Phẩm"
        );

    //B2: lưu info xuống database(cơ sở dữ liệu)
    if (isValid) {
        getELE("TenSP").value = "";
        getELE("GiaSP").value = "";
        getELE("HinhSP").value = "";
        getELE("MoTa").value = "";


        sanPham.themSP(sp)
            .then(function (result) {
                //Load lại danh sách sau khi thêm thành công 

                getListProducts();

                //gọi sự kiên click có sẵn của close button
                //Để tắt modal khi thêm thành công
                document.querySelector("#myModal .close").click();
            })
            .catch(function (error) {

                console.log(error);
            });
    }
}

function xoaSP(id) {
    batLoading()
    sanPham.xoaSanPham(id)
        .then(function (result) {
            //Load lại danh sách sau khi xóa thành công  
            tatLoading()
            getListProducts();

        })
        .catch(function (error) {
            tatLoading()
            console.log(error);
        });

}

function xemSP(id) {
    batLoading()
    sanPham.xemSanPham(id)
        .then(function (result) {
            tatLoading()
            console.log(result.data);
            //Mở modal 
            $('#myModal').modal('show');
            //Điền thông tin lên form
            getELE("TenSP").value = result.data.name;
            getELE("GiaSP").value = result.data.price
            getELE("HinhSP").value = result.data.img;
            getELE("MoTa").value = result.data.desc;

            //Thêm button cập nhật cho form
            var footerEle = document.querySelector(".modal-footer");
            footerEle.innerHTML = `
            <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              Close
            </button>
            <button onclick="capNhatSP('${result.data.id}')" class="btn btn-primary">Update Product</button>
        `;
        })
        .catch(function (error) {
            tatLoading()
            console.log(error);
        });

}

function capNhatSP(id) {
    //B1: Lấy thông tin(info) từ form
    var name = getELE("TenSP").value;
    var price = getELE("GiaSP").value;
    var img = getELE("HinhSP").value;
    var desc = getELE("MoTa").value;

    var sp = new SanPham(name, price, img, desc);
    var isValid =
        validation.kiemTraRong(
            name,
            "spanName",
            "Vui Lòng Nhập Tên Sản Phẩm"
        )

    isValid =
        isValid &
        validation.kiemTraRong(
            price,
            "spanPrice",
            "Vui Lòng Nhập Giá Sản Phẩm"
        ) &&
        validation.kiemTraGia(
            price,
            "spanPrice",
            "Giá phải từ 1,000,000VND-100,000,000VND",
            1000000,
            100000000
        );
    isValid =
        isValid &
        validation.kiemTraRong(
            img,
            "spanImg",
            "Vui Lòng Nhập Hình Ảnh Sản Phẩm"
        );
    isValid =
        isValid &
        validation.kiemTraRong(
            desc,
            "spanDesc",
            "Vui Lòng Nhập Mô Tả Sản Phẩm"
        );

    if (isValid) {
        getELE("TenSP").value = "";
        getELE("GiaSP").value = "";
        getELE("HinhSP").value = "";
        getELE("MoTa").value = "";
        sanPham.capNhatSanPham(id, sp)
            .then(function (result) {
                console.log(result.data);
                //Load lại danh sách sau khi cập nhật thành công    
                getListProducts();

                //gọi sự kiên click có sẵn của close button
                //Để tắt modal khi cập nhật thành công
                document.querySelector("#myModal .close").click();
            })
            .catch(function (error) {
                console.log(error);
            });

    }

}

function batLoading() {
    document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
    document.getElementById("loading").style.display = "none";
}
